//
//  main.cpp
//  LongestPrefixMatch
//
//  Created by Joshua Engelsma on 10/31/14.
//  Copyright (c) 2014 Joshua Engelsma. All rights reserved.
//

#include <iostream>
#include <vector>
#include <fstream>
#include <arpa/inet.h>
#include <cmath>
#include <chrono>

using namespace std;
class Node{
public:
    int currLength;
    string ipAddress;
    Node* children [8];
};

class Timer
{
public:
    Timer() : beg_(clock_::now()) {}
    void reset() { beg_ = clock_::now(); }
    double elapsed() const {
        return std::chrono::duration_cast<second_>
        (clock_::now() - beg_).count(); }
    
private:
    typedef std::chrono::high_resolution_clock clock_;
    typedef std::chrono::duration<double, std::ratio<1> > second_;
    std::chrono::time_point<clock_> beg_;
};

class Tree{
public:
    Node* rootPtr;
    
    /* constructor */
    Tree () {
        Node* rootptr = new Node();
        rootPtr = rootptr;
    }
    
    bool insert (string line, int strideLength) {
        //find the mask of your prefix header
        size_t firstPipe = line.find("|");
        size_t slash = line.find("/");
        size_t maskLength = firstPipe - slash-1;
        string maskStr = line.substr(slash+1, maskLength);
        int mask = atoi(maskStr.c_str());
        
        //find the prefix
        string prefix = line.substr(0, slash);
        struct in_addr addr;
        const char* cStrPrefix = prefix.c_str();
        inet_aton(cStrPrefix, &addr);
        addr.s_addr = ntohl(addr.s_addr); //return into network order
        
        //find ipAddress to insert...
        size_t secondPipe = line.find("|", firstPipe+1);
        size_t addressLength = line.length() - secondPipe-1;
        string ipAddress = line.substr(secondPipe+1, addressLength);
        int bitNumber = 0;
        
        
        //insert node using private helper function
        return insert(ipAddress, addr, bitNumber, mask, strideLength, rootPtr);
    }
    
    bool insert (string ipAddress, struct in_addr prefixAsInt, int bitNumber, int mask, int strideLength, Node* pos){
        int currBit = getBitValue(prefixAsInt, bitNumber, strideLength);
        if(strideLength == 1 && bitNumber == mask-1){ //base case for 1 bit trie
            if (pos->children[currBit] == nullptr){
                Node * child = new Node();
                child->ipAddress = ipAddress;
                child->currLength = mask;
                pos->children[currBit] = child;
                return true;
            } else{
                if(pos->children[currBit]->currLength < mask){ //if nodes mask is less than curr mask we can replace
                    pos->children[currBit]->ipAddress = ipAddress;
                    pos->children[currBit]->currLength = mask; //go with longer mask
                    return true;
                }
                return false; //insertion returns false because correct ip address in place
            }
            
        }else if(strideLength == 2 && mask % 2 == 0 && bitNumber == mask - 2){ //base case for even masked 2 bit trie
            currBit = getBitValue(prefixAsInt, bitNumber, strideLength);
            if (pos->children[currBit] == nullptr){
                Node* child = new Node();
                child->ipAddress = ipAddress;
                child->currLength = mask;
                pos->children[currBit] = child;
                return true;
            }else{
                if(pos->children[currBit]->currLength < mask){ //if nodes mask is less than curr mask we can replace
                    pos->children[currBit]->ipAddress = ipAddress;
                    pos->children[currBit]->currLength = mask;
                    return true;
                }
                return false; //insertion fails because correct ip address in place already
            }
        }else if(strideLength == 2 && mask % 2 != 0 && bitNumber == mask - 1){ //base case for non-even masked 2 bit trie
            currBit = getBitValue(prefixAsInt, bitNumber, 1); //just get the last bit
            vector<int> indexesToInsert = getIndexesForTwoTrie(currBit);
            for(int i = 0; i < indexesToInsert.size(); ++i){
                if (pos->children[indexesToInsert[i]] == nullptr){
                    Node* child = new Node();
                    child->ipAddress = ipAddress;
                    child->currLength = mask;
                    pos->children[indexesToInsert[i]] = child;
                }else{
                    if(pos->children[indexesToInsert[i]]->currLength < mask){ //if nodes mask is less than curr mask we can replace
                        pos->children[indexesToInsert[i]]->ipAddress = ipAddress;
                        pos->children[indexesToInsert[i]]->currLength = mask;
                    }
                }
            }
            return true;
        }else if(strideLength == 3 && mask % 3 == 0 && bitNumber == mask - 3){ //base case for even masked 3 bit trie
            currBit = getBitValue(prefixAsInt, bitNumber, strideLength);
            if (pos->children[currBit] == nullptr){
                Node* child = new Node();
                child->ipAddress = ipAddress;
                child->currLength = mask;
                pos->children[currBit] = child;
                return true;
            }else{
                if(pos->children[currBit]->currLength < mask){ //if nodes mask is less than curr mask we can replace
                    pos->children[currBit]->ipAddress = ipAddress;
                    pos->children[currBit]->currLength = mask;
                    return true;
                }
                return false; //insertion fails because correct ip address in place already
            }
        }else if(strideLength == 3 && mask % 3 != 0 && bitNumber == mask - 2){ //base case for non-even 1 bit missing 3 bit trie
            currBit = getBitValue(prefixAsInt, bitNumber, 2); //just get the last bit
            vector<int> indexesToInsert = getIndexesForThreeTrieFromTwo(currBit);
            for(int i = 0; i < indexesToInsert.size(); ++i){
                if (pos->children[indexesToInsert[i]] == nullptr){
                    Node* child = new Node();
                    child->ipAddress = ipAddress;
                    child->currLength = mask;
                    pos->children[indexesToInsert[i]] = child;
                }else{
                    if(pos->children[indexesToInsert[i]]->currLength < mask){ //if nodes mask is less than curr mask we can replace
                        pos->children[indexesToInsert[i]]->ipAddress = ipAddress;
                        pos->children[indexesToInsert[i]]->currLength = mask;
                    }
                }
            }
            return true;
            
        }else if(strideLength == 3 && mask % 3 != 0 && bitNumber == mask - 1){ //base case for non-even 2 bit missing 3 bit trie
            currBit = getBitValue(prefixAsInt, bitNumber, 1); //just get the last bit
            vector<int> indexesToInsert = getIndexesForThreeTrieFromOne(currBit);
            for(int i = 0; i < indexesToInsert.size(); ++i){
                if (pos->children[indexesToInsert[i]] == nullptr){
                    Node* child = new Node();
                    child->ipAddress = ipAddress;
                    child->currLength = mask;
                    pos->children[indexesToInsert[i]] = child;
                }else{
                    if(pos->children[indexesToInsert[i]]->currLength < mask){ //if nodes mask is less than curr mask we can replace
                        pos->children[indexesToInsert[i]]->ipAddress = ipAddress;
                        pos->children[indexesToInsert[i]]->currLength = mask;
                    }
                }
            }
            return true;
        } else{ //have not reached base case yet...recurse.
            int currBit = getBitValue(prefixAsInt, bitNumber, strideLength);
            bitNumber += strideLength;
            if (pos->children[currBit] == nullptr){
                Node* emptyNode = new Node();
                pos->children[currBit] = emptyNode;
            }
            return insert(ipAddress, prefixAsInt, bitNumber, mask, strideLength, pos->children[currBit]);
        }
        return false;
    }
    
    string findIpAddress(string prefix, int strideLength){
        //convert ip address to integer
        struct in_addr addr;
        const char* cStrPrefix = prefix.c_str();
        inet_aton(cStrPrefix, &addr);
        addr.s_addr = ntohl(addr.s_addr); //return into network order
        string tempIP = "No Match";
        
        //call private helper to find ip address
        string desiredIP = findIpAddress(addr, tempIP, 0, strideLength,rootPtr);
        return desiredIP;
    }
    
    string findIpAddress(struct in_addr prefixAsInt, string tempIP,int bitNumber, int strideLength, Node* pos){
        int currentBit = getBitValue(prefixAsInt, bitNumber, strideLength);
        if (pos->ipAddress != ""){ //update tempIP if possible with current node ip
            tempIP = pos->ipAddress;
        }
        if(pos->children[currentBit] == nullptr){ //no where left to traverse..
            return tempIP;
        }else if(pos->children[currentBit] != nullptr){
            bitNumber += strideLength;
            return findIpAddress(prefixAsInt, tempIP, bitNumber, strideLength, pos->children[currentBit]);
        }else{
            return "No Match";
        }
        
    }
    
    int getBitValue(struct in_addr prefixAsInt, int bitNumber, int strideLength)
    {
        unsigned int bitString;
        if (strideLength == 1){
            bitString = 1;
        }else if(strideLength == 2){
            bitString = 3;
        }else{
            bitString = 7;
        }
        
        int offset = strideLength - 1;
        int nbrOfShifts = (31 - bitNumber) - offset;
    
        prefixAsInt.s_addr = prefixAsInt.s_addr >> nbrOfShifts;
        int result = prefixAsInt.s_addr & bitString;
        return result;
    }
    
    vector<int> getIndexesForTwoTrie(int bit){
        vector<int> result;
        if (bit == 1){
            result.push_back(2);
            result.push_back(3);
        }else if(bit == 0){
            result.push_back(0);
            result.push_back(1);
        }
        return result;
    }
    
    vector<int> getIndexesForThreeTrieFromOne(int bit){
        vector<int> result;
        if (bit == 1){
            result.push_back(4);
            result.push_back(5);
            result.push_back(6);
            result.push_back(7);
        }else if(bit == 0){
            result.push_back(0);
            result.push_back(1);
            result.push_back(2);
            result.push_back(3);
        }
        return result;
    }
    
    vector<int> getIndexesForThreeTrieFromTwo(int bit){
        vector<int> result;
        if(bit == 0){
            result.push_back(0);
            result.push_back(1);
        }else if(bit == 1){
            result.push_back(2);
            result.push_back(3);
        }else if(bit == 2){
            result.push_back(4);
            result.push_back(5);
        }else if(bit == 3){
            result.push_back(6);
            result.push_back(7);
        }
        return result;
    }

};


string compareFileLines(string currentMin, string candidateMin){
    /*method counts number of whitespaces in between the sets of pipes...
     the string with least white spaces is the min
     */
    size_t currIndex = currentMin.find("|");
    size_t endIndex = currentMin.find("|", currIndex+1);
    string Ases = currentMin.substr(currIndex+1, endIndex-currIndex-1);
    int nbrOfWhiteSpacesForCurr = 0;
    currIndex = 0;
    endIndex = Ases.length() - 1;
    while (currIndex != endIndex){
        char currChar = Ases[currIndex];
        if (currChar == ' '){
            nbrOfWhiteSpacesForCurr++;
        }
        currIndex++;
    }
    size_t candIndex = candidateMin.find("|");
    size_t candEndIndex = candidateMin.find("|", candIndex+1);
    string candAses = candidateMin.substr(candIndex+1, candEndIndex-candIndex-1);
    int nbrOfWhiteSpacesForCand = 0;
    candIndex = 0;
    candEndIndex = candAses.length() - 1;
    while (candIndex != candEndIndex){
        char currChar = candAses[candIndex];
        if (currChar == ' '){
            nbrOfWhiteSpacesForCand++;
        }
        candIndex++;
    }
    return nbrOfWhiteSpacesForCurr <= nbrOfWhiteSpacesForCand ? currentMin : candidateMin;
}

int main(int argc, const char * argv[]) {
    //go through our file and find nodes to insert
    Tree *tree = new Tree;
    
    ifstream input (argv[1]);
    ifstream ips (argv[2]);
    string line;
    string nextLine;
    string currMin;
    bool moreLinesToRead = true;
    int strideLength;
    cout << "Please Enter the stride length: \n";
    cin >> strideLength;
    getline(input, line); // read very first line in file
    vector<double> times;
    while(moreLinesToRead){
        //get first line of a new prefix block
        size_t firstPipe = line.find("|");
        string prefix = line.substr(0, firstPipe);
        currMin = line;
        while(true){
            if(!getline(input, nextLine)){
                moreLinesToRead = false;
                break;
            }
            size_t nFirstPipe = nextLine.find("|");
            string nPrefix = nextLine.substr(0, nFirstPipe);
            if(nPrefix != prefix){
                tree->insert(currMin, strideLength);
                line = nextLine;
                break;
            }else{ //see if the next line is smaller
                currMin = compareFileLines(currMin, nextLine);
            }
        }
    }
    string ip;
    while(getline(ips, ip)){
        Timer *timer = new Timer();
        string ipAddress = tree->findIpAddress(ip, strideLength);
        double elapsed = timer->elapsed();
        times.push_back(elapsed);
        cout << ip << "  " <<ipAddress << "\n";
    }
    double totalTime = 0;
    for(auto iter = times.begin(); iter != times.end(); ++iter){
        totalTime += (*iter);
    }
    cout << "Average Lookup Time for Stride Length " << strideLength << " " << (totalTime/times.size()) << "\n";
    
    return 0;
}
